import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatButtonToggleModule,
    // MatDatepickerModule,
    // MatDialogModule,
    // MatExpansionModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatNativeDateModule,
    // MatPaginatorModule,
    // MatProgressBarModule,
    // MatProgressSpinnerModule,
    // MatRadioModule,
    // MatSelectModule,
    // // MatSliderModule,
    // MatSlideToggleModule,
    // MatSnackBarModule,
    // MatSortModule,
    // MatTableModule,
    // MatTabsModule,
    // MatTooltipModule,
} from '@angular/material';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, MatButtonModule,
    MatButtonToggleModule,],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
